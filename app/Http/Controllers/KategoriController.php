<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //kita gunakan Model Kategori dapatkan
        //data daripada table kategori
        //rekod2 tersebut dimasukkan di dalam
        //variable $kategori
        
        $kategori = Kategori::where('nama','like', '%' . request()->search . '%')
                        ->orWhere('kod','like', '%' . request()->search . '%')
                        //->with('permit') //eager loading 
                        ->paginate(10);
    

        //panggil view untuk paparkan rekod2
        return view('kategori.index', [
            'kategorias' => $kategori,
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //paparkan form untuk memasukkan rekod kategori
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        request()->validate([
            'kod' => ['unique:kategori', 'max:5', 'required'],
            'nama' => ['unique:kategori', 'required'],
        ]);

        //create rekod
        $kategori = new Kategori();

        //isi rekod dengan data disubmit
        $kategori->kod = request()->kod;
        $kategori->nama = request()->nama;
        
        //save
        $kategori->save();

        session()->flash('alert', 'Rekod Berjaya Disimpan');

        //redirect ke index (listing kategori)
        return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function show(Kategori $kategori)
    {
        return view('kategori.show', [
            'kategori' => $kategori
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function edit(Kategori $kategori)
    {
        return view('kategori.edit', [
            'kategori' => $kategori
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategori $kategori)
    {
        //validate
        request()->validate([
            'kod' => [ 'max:5', 'required', Rule::unique('kategori')->ignore($kategori->id) ],
            'nama' => [ 'required', Rule::unique('kategori')->ignore($kategori->id) ],
        ]);

        //isi rekod dengan data disubmit
        $kategori->kod = request()->kod;
        $kategori->nama = request()->nama;
        
        //save
        $kategori->save();

        session()->flash('alert', 'Rekod Berjaya Dikemaskini');

        //redirect ke index (listing kategori)
        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kategori  $kategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategori $kategori)
    {
        $kategori->delete();

        session()->flash('alert', 'Rekod Berjaya Dibuang');

        return redirect('/kategori');
    }
}
