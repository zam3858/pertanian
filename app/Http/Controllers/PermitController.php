<?php

namespace App\Http\Controllers;

use App\Models\Permit;
use App\Models\Negeri;
use App\Models\Kategori;
use App\Mail\PermitCreated;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Barryvdh\DomPDF\Facade as PDF;

class PermitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //kita gunakan Model Permit dapatkan
        //data daripada table permits
        //rekod2 tersebut dimasukkan di dalam
        //variable $permit
        //eager loading kategori
        $permit = Permit::where('nama','like', '%' . request()->search . '%')
                        ->orWhere('kod','like', '%' . request()->search . '%')
                        ->orWhereHas('negeri', function($query) {
                            $query->where('nama', request()->search);
                        })
                        ->with(['kategori','negeri'])
                        ->paginate(10);

        
        //panggil view untuk paparkan rekod2
        return view('permit.index', [
            'permitas' => $permit,
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $negeri2 = Negeri::orderBy('nama')
                            ->get();

        $kategori2 = Kategori::orderBy('nama')
                            ->get();


        //paparkan form untuk memasukkan rekod permit
        return view('permit.create', compact('negeri2','kategori2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd(request()->negeri);
        //validate
        request()->validate([
            'kod' => ['unique:permits', 'max:5', 'required'],
            'nama' => ['unique:permits', 'required'],
        ]);

        //create rekod
        $permit = new Permit();

        //isi rekod dengan data disubmit
        $permit->kod = request()->kod;
        $permit->nama = request()->nama;
        $permit->kategori_id = request()->kategori_id;
        
        //save
        $permit->save();

        $permit->negeri()->sync(request()->negeri);

        session()->flash('alert', 'Rekod Berjaya Disimpan');

        \Mail::to(auth()->user())
                ->send(new PermitCreated('Abu', $permit));

        //redirect ke index (listing permit)
        return redirect('/permit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permit  $permit
     * @return \Illuminate\Http\Response
     */
    public function show(Permit $permit)
    {
        $pdf = PDF::loadView('permit.print', [
            'permit' => $permit
        ]);

        return $pdf->download('permit_rekod');
    }

    public function show2($param = null) {
        echo "show2";
    }

    public function addition($number1, $number2) {
        return $number1 + $number2;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permit  $permit
     * @return \Illuminate\Http\Response
     */
    public function edit(Permit $permit)
    {
        
        $negeri2 = Negeri::orderBy('nama')
                            ->get();
        return view('permit.edit', [
            'permit' => $permit,
            'negeri2' => $negeri2
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permit  $permit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permit $permit)
    {
        //validate
        request()->validate([
            'kod' => [ 'max:5', 'required', Rule::unique('permits')->ignore($permit->id) ],
            'nama' => [ 'required', Rule::unique('permits')->ignore($permit->id) ],
        ]);

        //isi rekod dengan data disubmit
        $permit->kod = request()->kod;
        $permit->nama = request()->nama;
        
        //save
        $permit->save();

        $permit->negeri()->sync(request()->negeri);

        session()->flash('alert', 'Rekod Berjaya Dikemaskini');

        //redirect ke index (listing permit)
        return redirect('/permit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permit  $permit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permit $permit)
    {
        $permit->delete();

        session()->flash('alert', 'Rekod Berjaya Dibuang');

        return redirect('/permit');
    }
}
