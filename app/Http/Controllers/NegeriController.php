<?php

namespace App\Http\Controllers;

use App\Models\Negeri;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class NegeriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //kita gunakan Model Negeri dapatkan
        //data daripada table negeri
        //rekod2 tersebut dimasukkan di dalam
        //variable $negeri

        $negeri = Negeri::where('nama','like', '%' . request()->search . '%')
                        ->orWhere('kod','like', '%' . request()->search . '%')
                        ->paginate(2);
    

        //panggil view untuk paparkan rekod2
        return view('negeri.index', [
            'negerias' => $negeri,
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //paparkan form untuk memasukkan rekod negeri
        return view('negeri.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        request()->validate([
            'kod' => ['unique:negeri', 'max:5', 'required'],
            'nama' => ['unique:negeri', 'required'],
        ]);

        //create rekod
        $negeri = new Negeri();

        //isi rekod dengan data disubmit
        $negeri->kod = request()->kod;
        $negeri->nama = request()->nama;
        
        //save
        $negeri->save();

        session()->flash('alert', 'Rekod Berjaya Disimpan');

        //redirect ke index (listing negeri)
        return redirect('/negeri');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Negeri  $negeri
     * @return \Illuminate\Http\Response
     */
    public function show(Negeri $negeri)
    {
        return view('negeri.show', [
            'negeri' => $negeri
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Negeri  $negeri
     * @return \Illuminate\Http\Response
     */
    public function edit(Negeri $negeri)
    {
        return view('negeri.edit', [
            'negeri' => $negeri
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Negeri  $negeri
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Negeri $negeri)
    {
        //validate
        request()->validate([
            'kod' => [ 'max:5', 'required', Rule::unique('negeri')->ignore($negeri->id) ],
            'nama' => [ 'required', Rule::unique('negeri')->ignore($negeri->id) ],
        ]);

        //isi rekod dengan data disubmit
        $negeri->kod = request()->kod;
        $negeri->nama = request()->nama;
        
        //save
        $negeri->save();

        session()->flash('alert', 'Rekod Berjaya Dikemaskini');

        //redirect ke index (listing negeri)
        return redirect('/negeri');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Negeri  $negeri
     * @return \Illuminate\Http\Response
     */
    public function destroy(Negeri $negeri)
    {
        $negeri->delete();

        session()->flash('alert', 'Rekod Berjaya Dibuang');

        return redirect('/negeri');
    }
}
