<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table='kategori';

    public function permit(){
        return $this->hasMany('App\Models\Permit','kategori_id','id');
    }
}
