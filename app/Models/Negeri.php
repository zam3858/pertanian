<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Negeri extends Model
{
    protected $table='negeri';

    public function permit() {
        return $this->belongsToMany('App\Models\Permit','negeri_permit',
                                        'negeri_id',
                                        'permit_id'
                                    );
    }
}
