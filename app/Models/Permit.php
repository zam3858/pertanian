<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    protected $table='permits';
    protected $primaryKey = 'id';
    public $incrementing = true; // table ni tak guna autoincrement
    //protected $keyType = 'string';
    protected $connection = 'mysql';

    protected $fillable = ['nama','kod'];

    public function kategori() {
        return $this->belongsTo('App\Models\Kategori','kategori_id','id');
    }

    public function negeri() {
        return $this->belongsToMany('App\Models\Negeri','negeri_permit',
                                        'permit_id',
                                        'negeri_id'
                                    );
    }
    
}