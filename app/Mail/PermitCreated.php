<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PermitCreated extends Mailable
{
    use Queueable, SerializesModels;

    
    public function __construct($created_by, $permit)
    {
        $this->created_by = $created_by;
        $this->permit = $permit;
    }
        
    public function build()
    {
        return $this->view('email.permitcreated',[
           'created_by' => $this->created_by,
           'permit' => $this->permit,
        ]);
    }
}
