# Laravel Training

# Trainer

    Hizam Mohd
    hizam@fotia.com.my
    0133518996

# Ref

    https://laracasts.com/series/php-for-beginners
    laravelfromscratch.com
    goalkicker.com
    phptherightway.com

    https://khirulnizam.com/shop/

 # Step Tetapan Kategori

  1) bina table menggunakan migration file
    
    php artisan make:model Kategori -a
 
  2) edit file migration dalam database/migration/. tambah 2 field nama dan kod. sila rujuk migration file untuk permit

  3) run php artisan migrate pada terminal untuk membina table dalam database

  4) kalau nama table bukan plural nama model ( bukan kategoris), buka app/Kategori.php dan letakkan nama table pada model tersebut.

     protected $table='kategori';

  5) edit controller KategoriController.php isikan fungsi2 kosong. boleh rujuk/tiru kod permit

  6) copy folder resource/views/permit jadikan resource/views/kategori

  7) edit file-file dalam resource/views/permit dan tukarkan wording dan nama variable 'permit' jadi 'kategori'

  8) tambah route dalam web.php
  

  abc