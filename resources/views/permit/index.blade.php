@extends('layouts.app')

@section('content')
    <h3> Senarai Permit </h3>
    <hr>

    @if($permitas->count() == 0)
        <a href="{{ url('/permit/create' ) }}?nama={{ request()->search }}" class="btn btn-primary">New Permit</a>
    @else
        <a href="{{ url('/permit/create' ) }}" class="btn btn-primary">New Permit</a>
    @endif
    
    <hr>
        <form>
            <div class="form-group">
                <input type="text" class="form-control" name="search" 
                    value="{{ request()->search }}">
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    <hr>
    <table class="table table-stripe">
        <tr>
            <th>Bil</th>
            <th>Kod</th>
            <th>Nama</th>
            <th>Tarikh Aktif</th>
            <th>Kategori</th>
            <th></th>
        </tr> 
    @foreach($permitas as $cur_permit)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $cur_permit->kod }}</td>
            <td>{{ $cur_permit->nama}}</td>
            <td>{{ $cur_permit->tarikh_aktif}}</td>
            <td>{{ $cur_permit->kategori->nama  ?? '' }}</td>
            <td>
                @foreach($cur_permit->negeri as $negeri)
                    {{ $negeri->nama }},
                @endforeach
            </td>
            <td>
                <a href="{{ url('/permit/' . $cur_permit->id .'/edit' ) }}" class="btn btn-primary">Edit</a>
                <a href="{{ url('/permit/' . $cur_permit->id ) }}" class="btn btn-primary">Show</a>
                
                <form method="POST" action="{{ url('/permit/' . $cur_permit->id ) }}"
                    style="float:right;"
                    onsubmit="return confirm('Adakah anda pasti untuk buang rekod ini?');"
                >
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach

    @if($permitas->count() == 0)
    <tr>
        <td colspan='3'> Tiada Rekod Dijumpai </td>
    </tr>
    @endif
    

    </table>

    {{ $permitas->links() }}
    
@endsection

@section('scripts')
    <script>
        $('div')
    </script>
@endsection