@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ url('/permit') }}">
    @csrf
    <div class="form-group">
        <label for="kod">Kod</label>
        <input type="text" class="form-control" id="kod" aria-describedby="kod" 
            name="kod"
            value="{{ old('kod') }}"
            >
        @error('kod')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" aria-describedby="nama"
            name="nama"
            value="{{ old('nama', request()->nama) }}"
            >
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="tarikh_aktif">Tarikh Aktif</label>
        <input type="text" class="form-control" id="tarikh_aktif" aria-describedby="tarikh_aktif"
            name="tarikh_aktif"
            value="{{ old('tarikh_aktif', request()->tarikh_aktif) }}"
            >
        @error('tarikh_aktif')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <label for="tarikh_aktif">Kategori</label>
        <select class="form-control" name="kategori_id">
            <option value="">Sila Pilih</option>
            @foreach($kategori2 as $kategori)
                <option value="{{ $kategori->id }}">
                    {{ $kategori->nama }}
                </option>
            @endforeach
        </select>
        @error('kategori_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <hr>
    @foreach($negeri2 as $negeri)
    <div class="form-group">
        <label for="tarikh_aktif">
            <input type="checkbox" name="negeri[]" value="{{ $negeri->id }}" />
            {{ $negeri->nama }}
        </label>
        
    </div>
    @endforeach
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection