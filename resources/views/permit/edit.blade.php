@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ url('/permit/' . $permit->id ) }}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="kod">Kod</label>
        <input type="text" class="form-control" id="kod" aria-describedby="kod" 
            name="kod"
            value="{{ old('kod', $permit->kod) }}"
            >
        @error('kod')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" aria-describedby="nama"
            name="nama"
            value="{{ old('nama', $permit->nama) }}"
            >
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>

    <hr>
    @foreach($negeri2 as $negeri)
    <div class="form-group">
        <label for="tarikh_aktif">
            <input type="checkbox" name="negeri[]" value="{{ $negeri->id }}" 
            @if(in_array($negeri->id, $permit->negeri->pluck('id')->toArray() ))
                checked
            @endif
            />
            {{ $negeri->nama }}
        </label>
    </div>
    @endforeach
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection