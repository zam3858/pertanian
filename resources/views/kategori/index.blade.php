@extends('layouts.app')

@section('content')
    <h3> Senarai Kategori </h3>
    <hr>
    @if($kategorias->count() == 0)
        <a href="{{ url('/kategori/create' ) }}?nama={{ request()->search }}" class="btn btn-primary">New Kategori</a>
    @else
        <a href="{{ url('/kategori/create' ) }}" class="btn btn-primary">New Kategori</a>
    @endif
    
    <hr>
        <form>
            <div class="form-group">
                <input type="text" class="form-control" name="search" 
                    value="{{ request()->search }}">
                
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    <hr>
    <table class="table table-stripe">
        <tr>
            <th>Kod</th>
            <th>Nama</th>
            <th>Permit</th>
            <th></th>
        </tr>
    @foreach($kategorias as $cur_kategori)
        <tr>
            <td>{{ $cur_kategori->kod }}</td>
            <td>{{ $cur_kategori->nama}}</td>
            <td>
                @foreach($cur_kategori->permit as $permit)
                    {{ $permit->nama }}, 
                @endforeach
            </td>
            <td>
                <a href="{{ url('/kategori/' . $cur_kategori->id .'/edit' ) }}" class="btn btn-primary">Edit</a>
                <a href="{{ url('/kategori/' . $cur_kategori->id ) }}" class="btn btn-primary">Show</a>
                
                <form method="POST" action="{{ url('/kategori/' . $cur_kategori->id ) }}"
                    style="float:right;"
                    onsubmit="return confirm('Adakah anda pasti untuk buang rekod ini?');"
                >
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach

    @if($kategorias->count() == 0)
    <tr>
        <td colspan='3'> Tiada Rekod Dijumpai </td>
    </tr>
    @endif
    

    </table>

    {{ $kategorias->links() }}

@endsection
