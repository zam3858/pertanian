@extends('layouts.app')

@section('content')

    <div class="form-group">
        <label for="kod">Kod</label>
        <input type="text" class="form-control-plaintext" id="kod" aria-describedby="kod" 
            name="kod"
            value="{{ $kategori->kod }}"
            >
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control-plaintext" id="nama" aria-describedby="nama"
            name="nama"
            value="{{ $kategori->nama }}"
            >
    </div>

@endsection