@extends('layouts.app')

@section('content')

    <div class="form-group">
        <label for="kod">Kod</label>
        <input type="text" class="form-control-plaintext" id="kod" aria-describedby="kod" 
            name="kod"
            value="{{ $negeri->kod }}"
            >
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control-plaintext" id="nama" aria-describedby="nama"
            name="nama"
            value="{{ $negeri->nama }}"
            >
    </div>

@endsection