@extends('layouts.app')

@section('content')

    <form method="POST" action="{{ url('/negeri/' . $negeri->id ) }}">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="kod">Kod</label>
        <input type="text" class="form-control" id="kod" aria-describedby="kod" 
            name="kod"
            value="{{ old('kod', $negeri->kod) }}"
            >
        @error('kod')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" id="nama" aria-describedby="nama"
            name="nama"
            value="{{ old('nama', $negeri->nama) }}"
            >
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>

@endsection