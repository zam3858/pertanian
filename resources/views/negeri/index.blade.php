@extends('layouts.app')

@section('content')
    <h3> Senarai Negeri </h3>
    <hr>
    @if($negerias->count() == 0)
        <a href="{{ url('/negeri/create' ) }}?nama={{ request()->search }}" class="btn btn-primary">New Negeri</a>
    @else
        <a href="{{ url('/negeri/create' ) }}" class="btn btn-primary">New Negeri</a>
    @endif
    
    <hr>
        <form>
            <div class="form-group">
                <input type="text" class="form-control" name="search" 
                    value="{{ request()->search }}">
                
            </div>
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    <hr>
    <table class="table table-stripe">
        <tr>
            <th>Kod</th>
            <th>Nama</th>
            <th>Permit</th>
            <th></th>
        </tr>
    @foreach($negerias as $cur_negeri)
        <tr>
            <td>{{ $cur_negeri->kod }}</td>
            <td>{{ $cur_negeri->nama}}</td>
            <td>
                @foreach($cur_negeri->permit as $permit)
                    {{ $permit->nama }},
                @endforeach
            </td>
            <td>
                <a href="{{ url('/negeri/' . $cur_negeri->id .'/edit' ) }}" class="btn btn-primary">Edit</a>
                <a href="{{ url('/negeri/' . $cur_negeri->id ) }}" class="btn btn-primary">Show</a>
                
                <form method="POST" action="{{ url('/negeri/' . $cur_negeri->id ) }}"
                    style="float:right;"
                    onsubmit="return confirm('Adakah anda pasti untuk buang rekod ini?');"
                >
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach

    @if($negerias->count() == 0)
    <tr>
        <td colspan='3'> Tiada Rekod Dijumpai </td>
    </tr>
    @endif
    

    </table>

    {{ $negerias->links() }}

@endsection
