<?php

use Illuminate\Support\Facades\Route;

// http://pertanian.test/
Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')
        //->prefix('moduletetapan') //http://pertanian.test/moduletetapan/permit/show2
        ->group( function() {
            Route::get('permit/show2', 'PermitController@show2');
            Route::resource('permit','PermitController');
            Route::resource('negeri','NegeriController');
            Route::resource('kategori','KategoriController'); 
        });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/kategori', 'KategoriController@index');
// Route::get('/kategori/create', 'KategoriController@create');
// Route::post('/kategori', 'KategoriController@store');
// Route::get('/kategori/{kategori}', 'KategoriController@show');
// Route::get('/kategori/{kategori}/edit', 'KategoriController@edit');
// Route::put('/kategori/{kategori}', 'KategoriController@update');
// Route::delete('/kategori/{kategori}', 'KategoriController@destroy');

//http://pertanian.test/query1
Route::get('/query1', function() {
    //select * from permits
    // where nama = 'Lesen Padi' 
    //      AND ( 
    //            tarikh_aktif > 2000
    //              OR id > 0 
    //          )
    return \App\Models\Permit::where('nama', 'Lesen Padi')
                                ->where(function($query) {
                                    $query->whereYear('tarikh_aktif','>', '2000')
                                    ->orWhere('id','>', 0);
                                })
                                ->get();
});

//http://pertanian.test/query2
Route::get('/query2', function() {
    return \App\Models\Permit::with('negeri')
                ->whereHas('negeri', function($query) {
                    $query->where('nama', 'Selangor');
                }
                )
                ->get();
});

//http://pertanian.test/query3
Route::get('/query3', function() {
    return \DB::select("SELECT * 
                        FROM permits 
                        WHERE id = ?
                        AND tarikh_aktif = ?
    
    ", [1,'2020-01-01']);

});